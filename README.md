# iOS Coding Challenge #

Language: **Swift 4.1**

Target: **iOS 11.4**

Created on **Xcode 9.4.1** (9F2000)

## Overview ##
---
This project contains a list of well-known (_and not-so-well-known_) car brands fetched from an API. A user can view a particular car brand from the list and "like" the brand.


## Challenges ##
---
1. Right now the `loadCarBrands` method is incomplete and not displaying the car brands from the API in the tableView. **Fix this**.
2. Tapping on a car brand cell should go to the `CarDetailVC` modally and include the `CarBrand` along for the ride. Implementation is up to you (properties, segues, etc).
3. Complete `CarDetailVC`:
	1. Display car brand in `carBrandLabel`.
	2. Implement the `likeButton`.
	3. Do a cool animation when the like button is pressed.
	4. Dismiss the view controller after animation is finished.
4. Any "liked" car brand cells should display "Liked" in the detailTextLabel once `CarDetailVC` dismisses.
5. **BONUS!** Fetch more car brands as the user scrolls down the tableView. The API and `fetchCarBrands` supports pagination, you may overload the method signature to support pagination.


## Notes ##
---
* Don't just have one final gigantic commit, pretend each challenge is a feature -- **your commits should demonstrate to us how you approach your work**.
* You shouldn't have to modify the `fetchCarBrands` call in `NetworkingManager`. If the call begins to fail, please let us know!
* The completed project should be able to compile and run upon downloading. If you find that you need to use third party libraries, please commit and include them.