//
//  NetworkingManager.swift
//  CodingChallenge
//
//  Created by Dan Huang on 8/12/18.
//  Copyright © 2018 DH. All rights reserved.
//

import Foundation

class NetworkingManager {
    
    static let sharedInstance = NetworkingManager()
    
    typealias GetCarBrandsCompletion = ([CarBrand], Int) -> Void
    
    func fetchCarBrands(page: Int = 0, pageSize: Int = 20, completion: @escaping GetCarBrandsCompletion) {
        
        let carResponseURLString = "http://api-aws-eu-qa-1.auto1-test.com/v1/car-types/manufacturer?page=\(page)&pageSize=\(pageSize)&wa_key=coding-puzzle-client-449cc9d"
        
        guard let carResponseURL = URL(string: carResponseURLString) else { return }
        
        URLSession.shared.dataTask(with: carResponseURL) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            do {
                let carResponse = try JSONDecoder().decode(CarResponse.self, from: data)
                var listOfBrands = [CarBrand]()
                for carBrand in carResponse.data {
                    let brandToAdd = CarBrand(brand: carBrand.value)
                    listOfBrands.append(brandToAdd)
                }
                completion(listOfBrands, carResponse.totalPageCount)
            } catch let error {
                print(error)
            }
            
        }.resume()
    }
    
}
