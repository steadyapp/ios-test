//
//  CarDetailVC.swift
//  CodingChallenge
//
//  Created by Dan Huang on 8/12/18.
//  Copyright © 2018 DH. All rights reserved.
//

import UIKit

class CarDetailVC: UIViewController {
    
    @IBOutlet weak var carBrandLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    var carBrand: CarBrand?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loadCarData() {
        // TODO: Display car brand in carBrandLabel
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        // TODO:
        // 1. Mark car brand as liked
        // 2. Do a cool animation with the likeButton
        // 3. Dismiss modal after animation is complete
    }
    
}
