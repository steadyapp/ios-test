//
//  CarBrandsTVC.swift
//  CodingChallenge
//
//  Created by Dan Huang on 8/12/18.
//  Copyright © 2018 DH. All rights reserved.
//

import UIKit

class CarBrandsTableViewController: UITableViewController {

    var carBrands = [CarBrand]()
    
    // TODO: You can add variables to support pagination
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load my wannabe car brand
        let danCarBrand = CarBrand(brand: "Dan's Exquisite Motor Vehicles", isLiked: true)
        carBrands.append(danCarBrand)
        
        // Load a crappy car brand
        let crappyCarBrand = CarBrand(brand: "Yugo")
        carBrands.append(crappyCarBrand)
        
        // Load actual car brands
        loadCarBrands()
    }
    
    // MARK: Car Brands API
    
    func loadCarBrands() {
        NetworkingManager.sharedInstance.fetchCarBrands { brands, totalPageCount in
            // TODO:
            // 1. Populate tableView with more car brands
            // 2. You may overload the method signatures to support pagination
        }
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carBrands.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandCell", for: indexPath)
        
        let carBrandObject = carBrands[indexPath.row]
        
        cell.textLabel?.text = carBrandObject.brand
        cell.detailTextLabel?.text = carBrandObject.isLiked == true ? "Liked" : ""
        cell.detailTextLabel?.textColor = UIColor.lightGray
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO: Pass car brand from whichever cell is clicked and present CarDetailVC modally
    }
    
    // TODO: When a user scrolls, continue to load more car brands until all of them are loaded. (Default loads 20 at a time)
}
