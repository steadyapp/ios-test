//
//  CarBrand.swift
//  CodingChallenge
//
//  Created by Dan Huang on 8/12/18.
//  Copyright © 2018 DH. All rights reserved.
//

import Foundation

struct CarBrand {
    let brand: String
    let isLiked: Bool
    
    init(brand: String, isLiked: Bool = false) {
        self.brand = brand
        self.isLiked = isLiked
    }
}
