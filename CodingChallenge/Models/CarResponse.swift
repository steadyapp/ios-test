//
//  CarResponse.swift
//  CodingChallenge
//
//  Created by Dan Huang on 8/12/18.
//  Copyright © 2018 DH. All rights reserved.
//

import Foundation

struct CarResponse: Codable {
    let page: Int
    let pageSize: Int
    let totalPageCount: Int
    let data: [String: String]
    
    enum CodingKeys: String, CodingKey {
        case page
        case pageSize
        case totalPageCount
        case data = "wkda"
    }
}
